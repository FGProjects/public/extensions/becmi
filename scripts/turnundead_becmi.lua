function onInit()
  Debug.console("turnundead_becmi.lua", "onInit", "BECMI turn undead online", true);
  ActionTurn.onRoll = onRoll;
  ActionsManager.registerResultHandler("turnundead", onRoll);
end

function onRoll(rSource, rTarget, rRoll)
  Debug.console("turnundead_becmi.lua","onRoll","start");
  ActionsManager2.decodeAdvantage(rRoll);

  local rMessage = ActionsManager.createActionMessage(rSource, rRoll);
  local nMaxHDTurned = 0;
  local nMaxHDDestroyed = 0;
  local aTurnDice = {};
  local aHDTurn = {};

  if rRoll.nTarget then
    local nTotal = ActionsManager.total(rRoll);
    local nTargetDC = tonumber(rRoll.nTarget) or 0;
    local nMaxLevel = DataCommonADND.nDefaultTurnUndeadMaxLevel;
    local nMaxTurnHD = DataCommonADND.nDefaultTurnUndeadMaxHD;
    local nClericLevel = nTargetDC or 1;
    if (nClericLevel < 1) then
      nClericLevel = 1;
    elseif (nClericLevel > DataCommonADND.nDefaultTurnUndeadMaxLevel) then
      nClericLevel = DataCommonADND.nDefaultTurnUndeadMaxLevel;
    end

    rMessage.text = rMessage.text .. "[as Level " .. nClericLevel .. "] " .. nTotal;
    local bTurnedSome = false;
    local sTurn = "";
    for i=1, nMaxTurnHD,1 do
      local rTurn = {};
      local nTurnValue = DataCommonADND.aTurnUndead[nClericLevel][i];
      --  0 = Cannot turn
      -- -1 = Turn
      -- -2 = Destroy
      -- -3 = Additional 1d6 creatures affected.
      -- -4 = Additional 1d6 creatures affected
      if nTurnValue ~= 0 and nTotal >= nTurnValue then
        rTurn.nTurnValue = nTurnValue; -- save turn value for this HD.
        rTurn.bTurn = true;
        local sTurnedResult = "(TURN)";
        if (nTurnValue == -1) then
          sTurnedResult = "(TURN!)";
        elseif (nTurnValue == -2) then
          sTurnedResult = "(DESTROY)";
          rTurn.bDestroy = true
        elseif (nTurnValue == -3) then
          sTurnedResult = "(DESTROY!)";
          rTurn.bDestroyPlus = true
        elseif (nTurnValue == -4) then
          sTurnedResult = "(DESTROY!!)";
          rTurn.bDestroySharp = true
        end

        local sTurnString = DataCommonADND.turn_name_index[i];
        -- grab the number before HD, match "2" 1-2HD or "10" for 10HD or "6" for 5-6HD.
        local sMaxHD = sTurnString:match("(%d+)HD");
        local nMaxHD = 0;
        if (sMaxHD ~= nil) then
          nMaxHD = tonumber(sMaxHD) or 0;
        end
        rTurn.nHD = nMaxHD;
        -- collected all the data into rTurn, now stuff into aHDTurn
        table.insert(aHDTurn,rTurn);

        local sTurnedType = "\r\n[" .. sTurnString;
        sTurn = sTurn .. sTurnedType .. sTurnedResult .. "]";
        bTurnedSome = true;
      end
    end

    if (bTurnedSome) then
      rMessage.font = "successfont";
      rMessage.icon = "chat_success";

      Debug.console("turnundead_becmi.lua","onRoll","sTurn=",sTurn);
      ChatManager.SystemMessage("TURN VALUE: " .. sTurn);
    end

    -- if we have a dice count we turned something so roll it
    if (bTurnedSome) then
      table.insert(aTurnDice,'d6');
      table.insert(aTurnDice,'d6');
      local aExtraTurn = {}
      table.insert(aExtraTurn,'d6');
      local aExtraExtraTurn = {}
      table.insert(aExtraExtraTurn,'d6');
      table.insert(aExtraExtraTurn,'d6');

      local nodeTargets = ActorManagerADND.getTargetNodes(rSource);
      local aTargets = nil;
      local bTurnedNPC = false;
      if (nodeTargets ~= nil) then
        -- sort targets by HD, low to high
        aTargets = ActionTurn.sortByLevel(nodeTargets);
        -- if we have targets then we proceed
        if (#aTargets > 0) then
          local aTurnedList = {};
          local nTurnBase = StringManager.evalDice(aTurnDice, 0);
          Debug.console("turnundead_becmi.lua","onRoll","Turned Slots, 2d6, nTurnBase=",nTurnBase);
          ChatManager.SystemMessage("TURNED SLOTS [" .. nTurnBase .. "]");

          -- flip through #aHDTurn
          for i=1, #aHDTurn do
            local nTurnExtra = 0;
            -- check if turn/destroy/destroy+
            local bDestroy = aHDTurn[i].bDestroy;
            local bDestroyPlus = aHDTurn[i].bDestroyPlus;
            local bDestroySharp = aHDTurn[i].bDestroySharp;
            if bDestroyPlus then
              -- destroy+ turn (add 1d6)
              nTurnExtra = StringManager.evalDice(aExtraTurn, 0);
              Debug.console("turnundead_becmi.lua","onRoll","Gained extra turn, 1d6, nTurnExtra=",nTurnExtra," For HD=",aHDTurn[i].nHD);
              ChatManager.SystemMessage("TURNED EXTRA SLOTS [" .. nTurnExtra .. "] for HD [" .. aHDTurn[i].nHD .. "]");
            end
            if bDestroySharp then
              -- destroy# turn (add 2d6)
              nTurnExtra = StringManager.evalDice(aExtraExtraTurn, 0);
              Debug.console("turnundead_becmi.lua","onRoll","Gained a extra turn, 2d6, nTurnExtra=",nTurnExtra," For HD=",aHDTurn[i].nHD);
              ChatManager.SystemMessage("TURNED EXTRA SLOTS [" .. nTurnExtra .. "] for HD [" .. aHDTurn[i].nHD .. "]");
            end
            -- flip through sorted Targets
            local aTurnIDs = {};
            for nID=1,#aTargets do
              local sNodeName = aTargets[nID];
              if (sNodeName ~= nil) then
                local node = DB.findNode(sNodeName);
                -- if target.HD <= aHDTurn.nHD
                --local nLevel = DB.getValue(node,"level",9999);
                local nNPCHD = CombatManagerADND.getNPCLevelFromHitDice(node)
                local sCreatureType = DB.getValue(node,"type",""):lower();
                -- if type undead and level lower than HD effected by turn and not already turned
                if (sCreatureType:match("undead") ~= nil and nNPCHD <= aHDTurn[i].nHD and not ActionTurn.getAlreadyTurned(aTurnedList,node)) then
                  -- if check if nTurnExtraUsed < nTurnExtra then nTurnExtraUsed +1 and mark target turned/destroyed
                  if (nTurnExtra > 0) then
                    bTurnedNPC = true;
                    nTurnExtra = nTurnExtra - 1;
                    ActionTurn.handleTurn(rSource, node,rMessage,(bDestroy or bDestroyPlus or bDestroySharp));
                    table.insert(aTurnedList,node);
                  -- check if nTurnBaseUsed < nTurnBase then nTurnBaseUsed +1 and mark target turned/destroyed
                  elseif (nTurnBase > 0) then
                    bTurnedNPC = true;
                    nTurnBase = nTurnBase - 1;
                    ActionTurn.handleTurn(rSource, node,rMessage,(bDestroy or bDestroyPlus or bDestroySharp));
                    table.insert(aTurnedList,node);
                  else
                    -- nothing, we have no more turn slots left.
                  end
                end -- sNodeName == nil
              end
            end -- next sorted target
          end -- next #aHDTurn
        else
          -- no targets
          rMessage.text = rMessage.text .. " [NO TARGETS]";
        end
      end
      if (not bTurnedNPC) then
        rMessage.font = "failfont";
        rMessage.icon = "chat_fail";
        rMessage.text = rMessage.text .. " [NOTHING TURNED!]";
      end
    else
      -- turned nothing.
      rMessage.font = "failfont";
      rMessage.icon = "chat_fail";
      rMessage.text = rMessage.text .. " [FAILED TURNING!]";
    end
  end
  Comm.deliverChatMessage(rMessage);
end