function onInit()
  Debug.console("saves_becmi.lua", "onInit", "BECMI saves online", true);
  ActionSave.setNPCSave = setNPCSave;
end

function setNPCSave(nodeEntry, sSave, nodeNPC)
    --Debug.console("manager_action_save.lua", "setNPCSave", "DataCommonADND.aWarriorSaves[nLevel][nSaveIndex]", DataCommonADND.aWarriorSaves[0][1]);    
    --Debug.console("manager_action_save.lua", "setNPCSave", sSave);

    local nSaveIndex = DataCommonADND.saves_table_index[sSave];

    --Debug.console("manager_action_save.lua", "setNPCSave", "DataCommonADND.saves_table_index[sSave]", DataCommonADND.saves_table_index[sSave]);
    
    --Debug.console("manager_action_save.lua", "setNPCSave", "nSaveIndex", nSaveIndex);
    
    local nSaveScore = 20;

    -- check if the monster has a 'savesAs' entry
    local sSavesAs = DB.getValue(nodeNPC, "savesAs", "");
    --Debug.console("saves_becmi.lua", "setNPCSave", "sSavesAs", sSavesAs);
    if (sSavesAs ~= "") then
        -- use BECMI monster saves
        --Debug.console("saves_becmi.lua", "setNPCSave", "DataCommonADND.aNPCSaves[sSavesAs]", DataCommonADND.aNPCSaves[sSavesAs]);
        if (DataCommonADND.aNPCSaves[sSavesAs] ~= nil) then
            nSaveScore = DataCommonADND.aNPCSaves[sSavesAs][nSaveIndex];
        end
    else
        --Debug.console("saves_becmi.lua", "setNPCSave", "hitdice calculation");
        -- use AD&D saves based on hit dice
        local sHitDice = DB.getValue(nodeNPC, "hitDice", "1");
        DB.setValue(nodeEntry,"hitDice","string", sHitDice);
        
        local nLevel = CombatManagerADND.getNPCLevelFromHitDice(nodeNPC);

        -- store it incase we wanna look at it later
        DB.setValue(nodeEntry, "level", "number", nLevel);
        
        --Debug.console("manager_action_save.lua", "setNPCSave", "nLevel", nLevel);
        
        if (nLevel > 17) then
            nSaveScore = DataCommonADND.aWarriorSaves[17][nSaveIndex];
        elseif (nLevel < 1) then
            nSaveScore = DataCommonADND.aWarriorSaves[0][nSaveIndex];
        else
            nSaveScore = DataCommonADND.aWarriorSaves[nLevel][nSaveIndex];
        --Debug.console("manager_action_save.lua", "setNPCSave", "DataCommonADND.aWarriorSaves[nLevel][nSaveIndex]", DataCommonADND.aWarriorSaves[nLevel][nSaveIndex]);
        end
    end

    --Debug.console("manager_action_save.lua", "setNPCSave", "nSaveScore", nSaveScore);
    
    DB.setValue(nodeEntry, "saves." .. sSave .. ".score", "number", nSaveScore);
    DB.setValue(nodeEntry, "saves." .. sSave .. ".base", "number", nSaveScore);

    --Debug.console("manager_action_save.lua", "setNPCSave", "setValue Done");

    return nSaveScore;
end
